//! Definitions of game objects i.e. [`Item`]s, [`Location`]s, [`Person`]s

use crate::language::text::*;
use crate::{Item, Location, Person, Takeable};

use paste::paste;

// Location {
//     name: $name_NAME,
//     description: $name_DESCRIPTION,
//     items: vec![],
//     adjacent: vec![],
//     persons: vec![],
// }
macro_rules! location {
	($name:tt) => {
		Location {
			name: &paste! { [<$name _NAME>] },
			description: &paste! { [<$name _DESCRIPTION>] },
			items: vec![],
			adjacents: vec![],
			persons: vec![],
		}
	};
}

pub fn locations() -> Vec<Location> {
	vec![location!(EMPTY_ROOM), location!(MARKET_PLACE)]
}

// Item {
//     name: $name_NAME,
//     description: $name_DESCRIPTION,
//     takeable: $takeable,
// }
macro_rules! item {
	($name:tt, $takeable:expr) => {
		Item {
			name: &paste! { [<$name _NAME>] },
			description: &paste! { [<$name _DESCRIPTION>] },
			takeable: $takeable,
		}
	};
}

pub fn items() -> Vec<Item> {
	vec![
		item!(GOLDEN_RING, Takeable::Always),
		item!(IRON_SWORD, Takeable::Always),
		item!(LEATHER_HELMET, Takeable::Always),
		item!(RUSTY_KEY, Takeable::Always),
		item!(KNIFE, Takeable::Always),
		item!(STEEL_PICKAXE, Takeable::Always),
		item!(ROCK, Takeable::Never),
	]
}

// Person {
//     name: $name_NAME,
//     description: $name_DESCRIPTION,
//     trades: vec![],
// }
macro_rules! person {
	($name:tt) => {
		Person {
			name: &paste! { [<$name _NAME>] },
			description: &paste! { [<$name _DESCRIPTION>] },
			trades: vec![],
		}
	};
}

pub fn persons() -> Vec<Person> {
	vec![person!(TRADER)]
}
