//! A randomly generated text adventure.

use std::hash::{Hash, Hasher};
use std::io::{BufRead, Write};
use std::time;

mod objects;

mod rng;

mod config;

mod hash;
use hash::FastHash64;

mod parse;
use parse::{ParseError, ParsedCommand};

mod language;
use language::*;

pub struct Player {
	name: String,
	location: usize,
	inventory: Vec<usize>,
}

impl Player {
	fn new(name: String, location: usize) -> Self {
		Player {
			name,
			location,
			inventory: Vec::new(),
		}
	}
}

pub struct Location {
	name: &'static Text,
	description: &'static Text,
	items: Vec<usize>,
	adjacents: Vec<usize>,
	persons: Vec<usize>,
}

pub struct Item {
	name: &'static Text,
	description: &'static Text,
	takeable: Takeable,
}

/// Indicates whether an [`Item`] can be taken.
pub enum Takeable {
	/// The [`Item`] can always be taken.
	Always,
	/// The [`Item`] can be taken after using it with `item_idx`.
	AfterUse { item_idx: usize },
	/// The [`Item`] can never be taken.
	Never,
}

pub struct Person {
	name: &'static Text,
	description: &'static Text,
	trades: Vec<(usize, usize)>,
}

pub struct Text {
	name: &'static str,
	english: Option<&'static str>,
	german: Option<&'static str>,
}

impl Text {
	/// Returns the entry for [`Language`].
	fn get(&self, language: Language) -> Option<&'static str> {
		match language {
			Language::English => self.english,
			Language::German => self.german,
		}
	}

	/// Returns the correct translation for [`Language`] if the it exists.
	/// Otherwise generates an error message.
	fn translate(&self, language: Language) -> String {
		if let Some(translation) = self.get(language) {
			String::from(translation)
		} else {
			format!(
				"[Missing \"{:?}\" translation for \"{}\"!]",
				language, self.name
			)
		}
	}
}

#[derive(Debug, Clone, Copy)]
pub enum Language {
	English,
	German,
}

/// Concatenates a list of [`String`]s using commas and the
/// [`LAST_LIST_SEPERATOR`](text::LAST_LIST_SEPERATOR).
///
/// # Example
///
/// ```
/// let list = vec!["test".to_owned(), "1".to_owned(), "2".to_owned()];
/// let concat = concat_list(list, Language::English);
///
/// assert_eq!(concat, "test, 1 and 2");
/// ```
fn concat_list(list: Vec<String>, language: Language) -> String {
	let mut res = String::with_capacity(list.len());

	if !list.is_empty() {
		res.push_str(&list[0]);

		if list.len() > 1 {
			for elem in &list[1..list.len() - 1] {
				res.push_str(", ");
				res.push_str(&elem);
			}

			res.push(' ');
			res.push_str(&text::LAST_LIST_SEPERATOR.translate(language));
			res.push(' ');
			res.push_str(&list[list.len() - 1]);
		}
	} else {
		res.push_str(&text::EMPTY_LIST.translate(language));
	}

	res
}

/// A lowercase [`String`].
pub struct Lowercase(String);

impl Lowercase {
	#[inline(always)]
	pub fn from_unchecked(s: String) -> Self {
		Lowercase(s)
	}

	pub fn strip_prefix<'a>(&'a self, prefix: &'a str) -> Option<Lowercase> {
		self.0
			.strip_prefix(prefix)
			.map(|stripped| Lowercase(stripped.to_owned()))
	}

	pub fn strip_suffix<'a>(&'a self, prefix: &'a str) -> Option<Lowercase> {
		self.0
			.strip_suffix(prefix)
			.map(|stripped| Lowercase(stripped.to_owned()))
	}

	pub fn trim(&self) -> Self {
		Lowercase(self.0.trim().to_owned())
	}

	#[inline(always)]
	pub fn is_empty(&self) -> bool {
		self.0.is_empty()
	}

	#[inline(always)]
	pub fn split<'a>(&'a self, pat: &'a str) -> core::str::Split<'a, &'a str> {
		self.0.split(pat)
	}
}

impl std::cmp::PartialEq<Lowercase> for Lowercase {
	fn eq(&self, other: &Lowercase) -> bool {
		self.0 == other.0
	}
}

impl std::fmt::Display for Lowercase {
	fn fmt(
		&self,
		f: &mut std::fmt::Formatter<'_>,
	) -> Result<(), std::fmt::Error> {
		self.0.fmt(f)
	}
}

impl AsRef<str> for Lowercase {
	fn as_ref(&self) -> &str {
		&self.0
	}
}

impl From<&str> for Lowercase {
	fn from(s: &str) -> Self {
		Lowercase(s.to_lowercase())
	}
}

struct World {
	items: Vec<Item>,
	locations: Vec<Location>,
	persons: Vec<Person>,
	player: Player,
}

impl World {
	fn generate(language: Language) -> std::io::Result<World> {
		let items = objects::items();
		let mut locations = objects::locations();
		let mut persons = objects::persons();

		let stdin = std::io::stdin();

		print!("{}", text::NAME_ENTER.translate(language));
		std::io::stdout().flush()?;
		let mut name = String::new();
		stdin.read_line(&mut name)?;
		let name = name.trim().to_owned();

		println!("{}", text::GENERATION_START.translate(language));
		let start_time = time::Instant::now();

		let mut hasher = FastHash64::new();
		name.hash(&mut hasher);
		let hash = hasher.finish();

		println!("Hash: 0x{:x}", hash);

		let mut rand = rng::RNG::with_seed(hash).unwrap_or_else(|| {
			// save to unwrap since it can only fail for hash == 0
			rng::RNG::with_seed(1).unwrap()
		});

		let locations_len = locations.len();

		let start_item_idx = rand.next_number() as usize % items.len();

		let start_room_idx = rand.next_number() as usize % locations_len;
		let start_room = &mut locations[start_room_idx];

		let start_room_item_idx = rand.next_number() as usize % items.len();
		if start_room_item_idx != start_item_idx {
			start_room.items.push(start_room_item_idx);
		}

		let adjacent_room_idx = rand.next_number() as usize % locations_len;
		if start_room_idx != adjacent_room_idx {
			start_room.adjacents.push(adjacent_room_idx);
		}

		let start_room_person_idx = rand.next_number() as usize % persons.len();
		let start_room_person = &mut persons[start_room_person_idx];
		let trade_reward_idx = rand.next_number() as usize % items.len();
		let trade_item_idx = rand.next_number() as usize % items.len();
		if trade_reward_idx != start_item_idx
			&& trade_reward_idx != start_room_item_idx
			&& trade_reward_idx != trade_item_idx
		{
			start_room_person
				.trades
				.push((trade_item_idx, trade_reward_idx));
		}

		start_room.persons.push(start_room_person_idx);

		let mut player = Player::new(name, start_room_idx);
		player.inventory.push(start_item_idx);

		let elapsed = start_time.elapsed().as_nanos();
		println!("{}", text::GENERATION_FINISH.translate(language));
		println!("Generation took {}ns", elapsed);

		Ok(World {
			items,
			locations,
			persons,
			player,
		})
	}

	/// Searches for an [`Item`] with `search_name` in the current player
	/// [`Location`].
	///
	/// Returns a touple containing an index into the
	/// [`item list`](Location::items) of the location and an index into
	/// [`World::items`] when found.
	fn find_in_location(
		&self,
		search_name: &Lowercase,
		language: Language,
	) -> Option<(usize, usize)> {
		let item_idx_list = &self.locations[self.player.location].items;
		self.find_in_item_idx_list(item_idx_list, search_name, language)
	}

	/// Searches for an [`Item`] with `search_name` in the current player
	/// [inventory](Player::inventory).
	///
	/// Returns a touple containing an index into the
	/// [`item list`](Location::items) of the location and an index into
	/// [`World::items`] when found.
	fn find_in_inv(
		&self,
		search_name: &Lowercase,
		language: Language,
	) -> Option<(usize, usize)> {
		let item_idx_list = &self.player.inventory;
		self.find_in_item_idx_list(item_idx_list, search_name, language)
	}

	fn find_in_item_idx_list(
		&self,
		item_idx_list: &[usize],
		search_name: &Lowercase,
		language: Language,
	) -> Option<(usize, usize)> {
		for (i, &item_idx) in item_idx_list.iter().enumerate() {
			let item = &self.items[item_idx];
			if let Some(item_name) = item.name.get(language) {
				if item_name.to_lowercase() == search_name.0 {
					return Some((i, item_idx));
				}
			}
		}

		None
	}
}

/// Start the game.
///
/// for ansi formatting:
/// https://docs.rs/console/0.7.5/src/console/utils.rs.html#468-491
///
/// ```text
/// GO <LOCATION>
/// LOOK [[AT] (<OBJECT> | <LOCATION> | <PERSON> | <ITEM>)]
/// USE (<OBJECT> | <ITEM> [WITH <OBJECT>])
/// TALK <PERSON>
/// GIVE <ITEM> TO <PERSON>
/// TAKE <OBJECT>
/// INVENTORY
/// HELP [<COMMAND>]
/// QUIT | ^D | STOP | END
/// ```
fn main() -> std::io::Result<()> {
	let language = config::LANGUAGE;

	let mut world = World::generate(language)?;

	println!("\nHello, {}!\n", world.player.name);

	'game_loop: for line in std::io::stdin().lock().lines() {
		println!();
		let parsed = ParsedCommand::try_from(line?, language);

		match parsed {
			Ok(command) => {
				match command {
					ParsedCommand::Go { location } => {
						if let Some(current) = world.locations
							[world.player.location]
							.name
							.get(language)
						{
							if current.to_lowercase() == location.0 {
								println!(
									"{}",
									text::ALREADY_THERE.translate(language)
								);
								println!();
								continue 'game_loop;
							}
						}

						for &adjacent_idx in
							&world.locations[world.player.location].adjacents
						{
							let adjacent_room = &world.locations[adjacent_idx];
							if let Some(name) = adjacent_room.name.get(language)
							{
								if name.to_lowercase() == location.0 {
									let desc = adjacent_room
										.description
										.translate(language);
									println!("{}", desc);
									println!();
									world.player.location = adjacent_idx;
									continue 'game_loop;
								}
							}
						}

						println!("Room {} not found", location);
					}

					ParsedCommand::Look { name } => {
						let location = &world.locations[world.player.location];

						// without parameters, just display current location
						if name.is_empty() {
							println!(
								"{}",
								location.description.translate(language)
							);

							let mut see_list = vec![];

							for item_idx in
								&world.locations[world.player.location].items
							{
								let item = &world.items[*item_idx];
								see_list.push(item.name.translate(language));
							}

							for person_idx in
								&world.locations[world.player.location].persons
							{
								let person = &world.persons[*person_idx];
								see_list.push(person.name.translate(language));
							}

							for location_idx in &world.locations
								[world.player.location]
								.adjacents
							{
								let location = &world.locations[*location_idx];
								see_list
									.push(location.name.translate(language));
							}

							let list = concat_list(see_list, language);
							println!("You can see {}.\n", list);
						} else {
							// look at object/ persons

							for item_idx in
								&world.locations[world.player.location].items
							{
								let item = &world.items[*item_idx];
								if let Some(item_name) = item.name.get(language)
								{
									if item_name.to_lowercase() == name.0 {
										println!(
											"{}",
											item.description
												.translate(language)
										);
										println!();
										continue 'game_loop;
									}
								}
							}

							for item_idx in &world.player.inventory {
								let item = &world.items[*item_idx];
								if let Some(item_name) = item.name.get(language)
								{
									if item_name.to_lowercase() == name.0 {
										println!(
											"{}",
											item.description
												.translate(language)
										);
										println!();
										continue 'game_loop;
									}
								}
							}

							for person_idx in
								&world.locations[world.player.location].persons
							{
								let person = &world.persons[*person_idx];
								if let Some(person_name) =
									person.name.get(language)
								{
									if person_name.to_lowercase() == name.0 {
										println!(
											"{}",
											person
												.description
												.translate(language)
										);
										println!();
										continue 'game_loop;
									}
								}
							}

							for location_idx in &world.locations
								[world.player.location]
								.adjacents
							{
								let location = &world.locations[*location_idx];
								if let Some(location_name) =
									location.name.get(language)
								{
									if location_name.to_lowercase() == name.0 {
										let desc = location
											.description
											.translate(language);
										print!("{}\n\n", desc);
										continue 'game_loop;
									}
								}
							}

							println!("Unknown object: {}\n", name);
						}
					}

					ParsedCommand::Use { item1, item2 } => {
						if let Some(item2) = item2 {
							println!("Using {} with {}!", item1, item2);
						} else {
							println!("Using {}!", item1);
						}
						println!();
					}

					ParsedCommand::Talk { person } => {
						println!("Talking with {}!", person);
						println!();
					}

					ParsedCommand::Give {
						item: item_search,
						person: person_search,
					} => {
						let search_res =
							world.find_in_inv(&item_search, language);

						if let Some((idx_idx, item_idx)) = search_res {
							for &person_idx in
								&world.locations[world.player.location].persons
							{
								let person = &world.persons[person_idx];
								let person_name =
									(*person).name.translate(language);

								if person_name.to_lowercase() == person_search.0
								{
									// found person
									let item_name = &world.items[item_idx]
										.name
										.translate(language);

									for &trade in &person.trades {
										if trade.0 == item_idx {
											// found trade
											println!(
												"Giving {} to {}!\n",
												item_name, person_name
											);
											world.player.inventory[idx_idx] =
												trade.1;

											let bought_item =
												&world.items[trade.1];
											let bought_name = bought_item
												.name
												.translate(language);
											println!(
												"You've received {}!\n",
												bought_name
											);
											continue 'game_loop;
										}
									}
									println!("No trade for {}\n", item_name);
									continue 'game_loop;
								}
							}
							println!(
								"Could not find person {} in location.\n",
								person_search
							);
						} else {
							println!(
								"Could not find item {} in inventory\n",
								item_search
							);
						}
					}

					ParsedCommand::Take { item: search } => {
						let search_res =
							world.find_in_location(&search, language);

						if let Some((idx_idx, item_idx)) = search_res {
							let item = &world.items[item_idx];
							let item_name = item.name.translate(language);

							if let Takeable::Always = item.takeable {
								println!("Taking {}.", item_name);

								world.player.inventory.push(item_idx);
								world.locations[world.player.location]
									.items
									.remove(idx_idx);
							} else {
								println!("You can't pick up {}.", item_name);
							}
						} else {
							println!("Could not find {}.", search);
						}

						println!();
					}

					ParsedCommand::Inventory => {
						println!("{}", text::INVENTORY.translate(language));
						let mut list = Vec::new();
						for &item_idx in &world.player.inventory {
							let item = &world.items[item_idx];
							list.push(item.name.translate(language));
						}
						let concat = concat_list(list, language);
						println!("You have {}.\n", concat);
					}

					ParsedCommand::Help { command } => {
						if let Some(command) = command {
							println!("just {}", command);
						} else {
							println!("Go, Look, Use, Talk, Give, Take, Inventory, Help, Exit");
						}
						println!();
					}

					ParsedCommand::Quit => {
						break 'game_loop;
					}

					ParsedCommand::Empty => {
						continue 'game_loop;
					}
				}
			}
			Err(error) => match error {
				ParseError::TooManyArguments => {
					println!("Too many arguments for command, try \"help\".\n")
				}
				ParseError::TooFewArguments => {
					println!("Too few arguments for command, try \"help\".\n")
				}
				ParseError::NoMatch => println!("Unknown command.\n"),
			},
		}
	}

	Ok(())
}
