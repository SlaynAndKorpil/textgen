pub mod commands {
	use crate::parse::*;
	use crate::{Language, Lowercase};

	macro_rules! command {
		($name:tt, $command:ident, $($lang:ident : $text:expr,)*) => {
			pub const $name: Command = Command {
				name: CommandName {$(
					$lang: $text,
				)*},
				parse: $command,
			};
		}
	}

	fn parse_go_command(
		parameters: Lowercase,
		language: Language,
	) -> ParseResult {
		let optional_name_component = match language {
			Language::English => "to",
			Language::German => "nach",
		};

		let location_name = if let Some(location) =
			parameters.strip_prefix(optional_name_component)
		{
			location.trim()
		} else {
			parameters
		};

		if !location_name.is_empty() {
			Ok(ParsedCommand::Go {
				location: location_name,
			})
		} else {
			Err(ParseError::TooFewArguments)
		}
	}

	fn parse_look_command(
		parameters: Lowercase,
		language: Language,
	) -> ParseResult {
		let name = match language {
			Language::English => {
				if let Some(stripped) = parameters.strip_prefix("at") {
					stripped.trim()
				} else {
					parameters
				}
			}
			Language::German => {
				if let Some(stripped) = parameters.strip_suffix("an") {
					stripped.trim()
				} else {
					parameters
				}
			}
		};

		Ok(ParsedCommand::Look { name })
	}

	fn parse_use_command(
		parameters: Lowercase,
		language: Language,
	) -> ParseResult {
		let seperator = match language {
			Language::English => "with",
			Language::German => "mit",
		};

		let mut item_list = parameters.split(seperator);

		let item1 = Lowercase::from_unchecked(
			item_list.next().unwrap().trim().to_owned(),
		);

		if item1.is_empty() {
			return Err(ParseError::TooFewArguments);
		}

		if let Some(item2) = item_list.next() {
			if item_list.next().is_some() {
				return Err(ParseError::TooManyArguments);
			}

			let item2 = item2.trim();
			if !item2.is_empty() {
				let item2 = Some(Lowercase::from_unchecked(item2.to_owned()));
				Ok(ParsedCommand::Use { item1, item2 })
			} else {
				Err(ParseError::TooFewArguments)
			}
		} else {
			Ok(ParsedCommand::Use { item1, item2: None })
		}
	}

	fn parse_talk_command(
		parameters: Lowercase,
		_language: Language,
	) -> ParseResult {
		if !parameters.is_empty() {
			Ok(ParsedCommand::Talk { person: parameters })
		} else {
			Err(ParseError::TooFewArguments)
		}
	}

	fn parse_give_command(
		parameters: Lowercase,
		language: Language,
	) -> ParseResult {
		let seperator = match language {
			Language::English => "to",
			Language::German => "an",
		};

		let mut param_list = parameters.split(seperator);

		let item = param_list.next().unwrap().trim();

		if item.is_empty() {
			return Err(ParseError::TooFewArguments);
		}

		if let Some(person) = param_list.next() {
			if param_list.next().is_some() {
				return Err(ParseError::TooManyArguments);
			}

			let person = person.trim();
			if !person.is_empty() {
				let item = Lowercase::from_unchecked(item.to_owned());
				let person = Lowercase::from_unchecked(person.to_owned());
				return Ok(ParsedCommand::Give { item, person });
			}
		}

		Err(ParseError::TooFewArguments)
	}

	fn parse_take_command(
		parameters: Lowercase,
		_language: Language,
	) -> ParseResult {
		if !parameters.is_empty() {
			Ok(ParsedCommand::Take { item: parameters })
		} else {
			Err(ParseError::TooFewArguments)
		}
	}

	fn parse_inventory_command(
		_parameters: Lowercase,
		_language: Language,
	) -> ParseResult {
		Ok(ParsedCommand::Inventory)
	}

	fn parse_help_command(
		parameters: Lowercase,
		_language: Language,
	) -> ParseResult {
		if !parameters.is_empty() {
			Ok(ParsedCommand::Help {
				command: Some(parameters),
			})
		} else {
			Ok(ParsedCommand::Help { command: None })
		}
	}

	fn parse_quit_command(
		_parameters: Lowercase,
		_language: Language,
	) -> ParseResult {
		Ok(ParsedCommand::Quit)
	}

	command!(
		GO,
		parse_go_command,
		english: "go",
		german:  "geh",
	);

	command!(
		LOOK,
		parse_look_command,
		english: "look",
		german:  "sieh",
	);

	command!(
		USE,
		parse_use_command,
		english: "use",
		german:  "verwende",
	);

	command!(
		TALK,
		parse_talk_command,
		english: "talk",
		german:  "rede",
	);

	command!(
		GIVE,
		parse_give_command,
		english: "give",
		german:  "übergebe",
	);

	command!(
		TAKE,
		parse_take_command,
		english: "take",
		german:  "nimm",
	);

	command!(
		INVENTORY,
		parse_inventory_command,
		english: "inventory",
		german:  "inventar",
	);

	command!(
		HELP,
		parse_help_command,
		english: "help",
		german:  "hilfe",
	);

	command!(
		QUIT,
		parse_quit_command,
		english: "exit",
		german:  "ende",
	);

	/// Definitions of all commands.
	pub const COMMANDS: &[Command] =
		&[GO, LOOK, USE, TALK, GIVE, TAKE, INVENTORY, HELP, QUIT];
}

pub mod text {
	use paste::paste;

	macro_rules! translations {
		($name:tt, $($lang:ident : $text:expr,)*) => {
			pub const $name: crate::Text = crate::Text {
				name: stringify!($name),
				$(
					$lang: $text,
				)*
			};
		}
	}

	/// The most beautiful macro ever.
	/// Definitely not over-complicated...
	macro_rules! name_and_description {(
			$name:tt,
			name: {$($name_lang:ident : $name_text:expr,)*},
			description: {$($desc_lang:ident : $desc_text:expr,)*},
		) => {
			paste! {
				pub const [<$name _NAME>]: crate::Text = crate::Text {
					name: stringify!([<$name _NAME>]),
					$(
						$name_lang: $name_text,
					)*
				};
				pub const [<$name _DESCRIPTION>]: crate::Text = crate::Text {
					name: stringify!([<$name _DESCRIPTION>]),
					$(
						$desc_lang: $desc_text,
					)*
				};
			}
		}
	}

	translations!(
		NAME_ENTER,
		english: Some("Please enter your name: "),
		german: Some("Bitte gib deinen Namen ein: "),
	);

	translations!(
		GENERATION_START,
		english: Some("Started world generation..."),
		german: Some("Weltgeneration gestarted..."),
	);

	translations!(
		GENERATION_FINISH,
		english: Some("Finished world generation."),
		german: Some("Weltgeneration abgeschlossen."),
	);

	translations!(
		LAST_LIST_SEPERATOR,
		english: Some("and"),
		german: Some("und"),
	);

	translations!(
		EMPTY_LIST,
		english: Some("nothing"),
		german: Some("nichts"),
	);

	translations!(
		ALREADY_THERE,
		english: Some("You are already there."),
		german: Some("Du bist schon da."),
	);

	translations!(
		INVENTORY,
		english: Some("Inventory:"),
		german: Some("Inventar:"),
	);

	name_and_description!(
		GOLDEN_RING,
		name: {
			english: Some("Golden Ring"),
			german: Some("Goldener Ring"),
		},
		description: {
			english: Some("A very shiny golden ring."),
			german: Some("Ein glänzender goldener Ring"),
		},
	);

	name_and_description!(
		LEATHER_HELMET,
		name: {
			english: Some("Leather Helmet"),
			german: Some("Lederhelm"),
		},
		description: {
			english: Some("A light and cheap helmet for a small amount of protection."),
			german: Some("Ein leichter und billiger Helm, der nur eine geringe Schutzmöglichkeit bietet."),
		},
	);

	name_and_description!(
		KNIFE,
		name: {
			english: Some("Knife"),
			german: Some("Messer"),
		},
		description: {
			english: Some("A common kitchen knife."),
			german: Some("Ein gewöhnliches Küchenmesser."),
		},
	);

	name_and_description!(
		RUSTY_KEY,
		name: {
			english: Some("Rusty Key"),
			german: Some("Verrosteter Schlüssel"),
		},
		description: {
			english: Some("It looks very old. What lock might it open?"),
			german: Some("Er sieht sehr alt aus. Zu welchem Schloss mag er wohl gehören?"),
		},
	);

	name_and_description!(
		STEEL_PICKAXE,
		name: {
			english: Some("Steel pickaxe"),
			german: Some("Stahlspitzhacke"),
		},
		description: {
			english: Some("The wooden handle is wet and part of it is already broken off, but the metal still seems durable."),
			german: Some("Der Holzgriff ist nass und Teile davon sind schon abgesplittert, aber das Metall scheint noch kräftig zu sein."),
		},
	);

	name_and_description!(
		ROCK,
		name: {
			english: Some("Big rock"),
			german: Some("Großer Stein"),
		},
		description: {
			english: Some("A big, heavy rock that seems unmoveable blocks your way."),
			german: Some("Der große, schwere Stein, der deinen Weg blockiert schein unbeweglich."),
		},
	);

	name_and_description!(
		IRON_SWORD,
		name: {
			english: Some("Iron Sword"),
			german: Some("Eisenschwert"),
		},
		description: {
			english: Some("A slightly rusted one-handed sword."),
			german: Some("Ein leicht verrostetes einhändiges Schwert."),
		},
	);

	name_and_description!(
		EMPTY_ROOM,
		name: {
			english: Some("An empty room"),
			german: Some("Ein leerer Raum"),
		},
		description: {
			english: Some("A very dark, empty room."),
			german: Some("Ein sehr dunkler, leerer Raum."),
		},
	);

	name_and_description!(
		MARKET_PLACE,
		name: {
			english: Some("Market Place"),
			german: Some("Marktplatz"),
		},
		description: {
			english: Some("The market place is filled with merchants, farmers, travellers, adventurers and folk of all kind trying to make money or resupply."),
			german: None,
		},
	);

	name_and_description!(
		TRADER,
		name: {
			english: Some("Trader"),
			german: Some("Händler"),
		},
		description: {
			english: Some("A small man standing in the corner of the room greets you. He's wearing a pink tunic with a yellow scarf, like many of the rich folk from Lãndïr, though his looks torn and smells like animal feces. \"Who are you, adventurer?\", he asks, \"Ah, whatever. I don't really care anyway. What I do care about is my @i, which got stolen by @e. Maybe you can bring it back? I've lost all my money on the robbery but if you meet me in Lãndïr I can give you @r."),
			german: None,
		},
	);
}
