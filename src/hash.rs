use std::convert::TryInto;

pub struct FastHash64 {
	hash: u64,
}

impl FastHash64 {
	/// Generate a new [`FastHash64`] initialized with `0`.
	pub fn new() -> Self {
		FastHash64 { hash: 0 }
	}
}

impl std::hash::Hasher for FastHash64 {
	fn finish(&self) -> u64 {
		self.hash
	}

	/// Hash function stolen from
	/// [github](https://github.com/ztanml/fast-hash/blob/master/fasthash.c)
	/// (`fasthash64()`).
	fn write(&mut self, bytes: &[u8]) {
		let m = 0x880355f21e6d1965u64;
		let end_aligned = bytes.len() & !7;
		let mut h = self.hash ^ (bytes.len() as u64).wrapping_mul(m);
		let mut v;

		for offset in (0..end_aligned).step_by(8) {
			v = u64::from_ne_bytes(
				bytes[offset..offset + 8].try_into().unwrap(),
			);
			h ^= mix(v);
			h = h.wrapping_mul(m);
		}

		v = 0;
		let mut shift = 0;
		for byte in &bytes[end_aligned..bytes.len()] {
			v ^= (*byte as u64) << shift;
			shift += 8;
		}
		if end_aligned != bytes.len() {
			h ^= mix(v);
			h = h.wrapping_mul(m);
		}

		self.hash = mix(h);
	}
}

#[inline]
fn mix(mut h: u64) -> u64 {
	h ^= h >> 23;
	h = h.wrapping_mul(0x2127599bf4325c37u64);
	h ^= h >> 47;
	h
}
