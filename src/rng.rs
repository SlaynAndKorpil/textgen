//! A random number generator.

/// A random number generator for [`u64`].
/// Using XorShift for fast and easy but insecure randomness.
#[allow(clippy::upper_case_acronyms)]
pub struct RNG {
	state: u64,
}

impl RNG {
	/// Creates a new [`RNG`] initialized with `seed`.
	///
	/// Returns [`None`] when `seed == 0` since this this would result in
	/// [`next_number`](RNG::next_number) always returning `0`.
	pub fn with_seed(seed: u64) -> Option<Self> {
		if seed == 0 {
			None
		} else {
			Some(RNG { state: seed })
		}
	}

	/// Generates the next number using XorShift.
	pub fn next_number(&mut self) -> u64 {
		let mut x = self.state;

		x ^= x << 13;
		x ^= x >> 7;
		x ^= x << 17;

		self.state = x;
		x
	}
}
